const express = require("express");
const app = express();
const body_parser = require("body-parser");

/**
 * @description Middleware of express js
 * */
app.use(body_parser.urlencoded({extended: false}));
app.use(body_parser.json());


/**
 * @router /
 * @method GET
 * @description Welcome  Home Page
 * */
app.get("/", (req, res) => {
    res.json(
        {
            message: "Welcome"
        }
    );
});

//Routes API Handler
const authManager = require("./routes/api/UserAuthentication");
app.use("/api/authentication", authManager);

/**
 *@description : At 3000 port number on server start
 */
const port = 3000;
app.listen(3000, () => {
    console.log(`Port ${port} is open`);
});