const express = require('express');
const router = express.Router();
const multer = require('multer');


/**
 *@description Multer Storage and File name configuration
 * */
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './images/userprofile');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + (file.originalname === undefined ? "undefined" : file.originalname));
    }
});
const upload = multer({storage: storage});

/**
 * @method POST
 * @router /api/authentication/createUser
 * @body image, fullname, email, password, contactno, bio, isdeleted, createddate, updateddate, user_type, user_device_type, user_device_token, user_device_id
 * @exports error => {message} or User Data
 * @description Create new user profile
 * @Test
 * */
router.post("/createUser", upload.single('image'), (req, res) => {
    res.json(
        {
            uimage:req.file.path,
            fullname:req.body.fullname,
            email:req.body.email,
            password:req.body.password,
            contactno:req.body.contactno,
            bio:req.body.bio,
            isdeleted:req.body.isdeleted,
            createddate:req.body.createddate,
            updateddate:req.body.updateddate,
            user_type:req.body.user_type,
            user_device_type:req.body.user_device_type,
            user_device_token:req.body.user_device_token,
            user_device_id:req.body.user_device_id
        }
    );
});

/**
 * @routes /api/authentication
 * @Test
 */
module.exports = router;